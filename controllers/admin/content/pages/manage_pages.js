/*
    Copyright (C) 2014  PencilBlue, LLC

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/**
 * Interface for managing pages
 */

function ManagePages() {}

//inheritance
util.inherits(ManagePages, pb.BaseController);

//statics
var SUB_NAV_KEY = 'manage_pages';

ManagePages.prototype.render = function (cb) {
    var self = this;

    var opts = {
        select: pb.DAO.PROJECT_ALL,
        where: pb.DAO.ANYWHERE,
        order: {
            headline: pb.DAO.ASC
        }
    };
    var dao = new pb.DAO();
    dao.q('page', opts, function (err, pages) {
        if (util.isError(err)) {
            return self.reqHandler.serveError(err);
        } else if (pages.length === 0) {
            return self.redirect('/admin/content/pages/new', cb);
        }

        pb.users.getAuthors(pages, function (err, pagesWithAuthor) {
            self.preparePageData(pagesWithAuthor, function (err, pageData) {
                if (err) {
                    cb(err);
                    return;
                }
                var angularObjects = pb.js.getAngularObjects({
                    navigation: pb.AdminNavigation.get(self.session, ['content', 'pages'], self.ls),
                    pills: pb.AdminSubnavService.get(SUB_NAV_KEY, self.ls, 'manage_pages'),
                    pages: pageData
                });

                var title = self.ls.get('MANAGE_PAGES');
                self.setPageName(title);
                self.ts.registerLocal('angular_objects', new pb.TemplateValue(angularObjects, false));
                self.ts.load('admin/content/pages/manage_pages', function (err, data) {
                    var result = '' + data;
                    cb({
                        content: result
                    });
                });

            });
        });
    });
};

ManagePages.prototype.preparePageData = function (pages, cb) {
    var self = this,
        newPages;
    newPages = self.getPageStatuses(pages);

    self.addPrivateAreaData(newPages, function (err, pagesWPrivateData) {
        if (err) {
            cb(err);
            return;
        }

        cb(null, pagesWPrivateData);
    });
};

ManagePages.prototype.addPrivateAreaData = function (pages, cb) {
    var self = this,
        actualUser = self.session.authentication.user_id;

    self.getPrivateAreas(function (err, privateAreas) {
        if (err) {
            cb(err);
            return;
        }
        var privateAreaMap = {};
        privateAreas.forEach(function (obj) {
            privateAreaMap[obj._id] = obj;
            return;
        });

        pages.forEach(function (page, index, array) {
            var privateAreaObject = privateAreaMap[page.private_area] || null,
                allowedUsers = privateAreaObject ? privateAreaObject.allowed_users : null,
                userMatch = null;

            page.allowed_user = true;
            page.private_area = page.private_area || null;
            page.private_area_name = privateAreaObject ? privateAreaObject.name : null;

            if (page.private_area && !privateAreaObject) {
                // if no name insert the id
                page.private_area_name = "NOTFOUND";
            }

            if (allowedUsers) {
                allowedUsers.forEach(function (allowedUser) {
                    if (allowedUser === actualUser) {
                        userMatch = allowedUser;
                    }
                });
            }

            // block user if there is private area
            // and user is not the author and is not in the private area allowed user list
            if (page.private_area && !userMatch && page.author !== actualUser) {
                page.allowed_user = false;
            }
        });

        cb(null, pages);
    });
};

/**
 * @static
 * @method getPrivateAreas
 *
 */
ManagePages.prototype.getPrivateAreas = function (cb) {
    var self = this,
        objectName = 'private_area',
        customObjectService = new pb.CustomObjectService();

    customObjectService.loadTypeByName(objectName, function (err, objectType) {
        if (!objectType) {
            pb.log.error('Custom object ' + objectName + ' does not exist');
            cb(err);
        } else {
            customObjectService.findByType(objectType, null, function (err, objectResults) {
                if (err) {
                    pb.log.error(err);
                    cb(err);
                    return;
                }
                cb(null, objectResults);
            });
        }
    });
};
ManagePages.prototype.getPageStatuses = function (pages) {
    var now = new Date();
    for (var i = 0; i < pages.length; i++) {
        if (pages[i].draft) {
            pages[i].status = this.ls.get('DRAFT');
        } else if (pages[i].publish_date > now) {
            pages[i].status = this.ls.get('UNPUBLISHED');
        } else {
            pages[i].status = this.ls.get('PUBLISHED');
        }
    }

    return pages;
};

ManagePages.getSubNavItems = function (key, ls, data) {
    return [{
        name: 'manage_pages',
        title: ls.get('MANAGE_PAGES'),
        icon: 'refresh',
        href: '/admin/content/pages'
    }, {
        name: 'new_page',
        title: '',
        icon: 'plus',
        href: '/admin/content/pages/new'
    }];
};

ManagePages.getRoutes = function (cb) {
    var routes = [{
        method: 'get',
        path: "/admin/content/pages",
        access_level: ACCESS_EDITOR,
        auth_required: true,
        content_type: 'text/html'
    }];
    cb(null, routes);
};

//register admin sub-nav
pb.AdminSubnavService.registerFor(SUB_NAV_KEY, ManagePages.getSubNavItems);

//exports
module.exports = ManagePages;
