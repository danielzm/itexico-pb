/*
 * Generic Interfase to get custom objects
 */

/**
 *
 */

function CustomObjectApi() {}

//inheritance
util.inherits(CustomObjectApi, pb.BaseController);

CustomObjectApi.prototype.render = function (mainCb) {
    var self = this,
        customObjectService = new pb.CustomObjectService(),
        objectName = this.pathVars.object;

    customObjectService.loadTypeByName(objectName, function (err, objectType) {
        if (!objectType) {
            pb.log.error('Custom object ' + objectName + ' does not exist');
            mainCb({
                content: pb.BaseController.apiResponse(pb.BaseController.API_FAILURE, null, err)
            });
        } else {
            self.getFullCustomObjects(objectType, function (err, results) {
                if (err) {
                    mainCb({
                        content: pb.BaseController.apiResponse(pb.BaseController.API_FAILURE, null, err)
                    });
                } else {
                    mainCb({
                        content: pb.BaseController.apiResponse(pb.BaseController.API_SUCCESS, null, results)
                    });
                }
            });
        }
    });
};

CustomObjectApi.prototype.getFullCustomObjects = function (objectType, mainCb) {
    var self = this,
        customObjectService = new pb.CustomObjectService();

    customObjectService.findByType(objectType, null, function (err, objectResults) {
        self.getChildrenForArrayOfCustomObjects(objectResults, objectType, mainCb);
    });
};

CustomObjectApi.prototype.getChildrenForArrayOfCustomObjects = function (coArray, coType, mainCb) {
    var fullObjects = [],
        customObjectService = new pb.CustomObjectService();

    var getObjectChildren = function (customObject, cb) {
        var options = {
            fetch_depth: 1
        };

        customObjectService.fetchChildren(customObject, options, coType, function (err, fullCustomObj) {
            if (err) {
                cb(err);
            } else {
                fullObjects.push(fullCustomObj);
                cb();
            }
        })
    };

    async.each(coArray, getObjectChildren, function (err, results) {
        mainCb(err, fullObjects);
    });
};

CustomObjectApi.getRoutes = function (cb) {
    var routes = [{
            method: 'get',
            path: '/api/custom_object/:object',
            auth_required: true,
            content_type: 'application/json'
        },
        // This will be implemented later
        {
            method: 'get',
            path: '/api/custom_object/:object/:id',
            auth_required: true,
            content_type: 'application/json'
        }
    ];

    cb(null, routes);
};


//exports
module.exports = CustomObjectApi;
