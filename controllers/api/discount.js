/*
 */

/**
 *
 */

function DiscountApi() {}

//inheritance
util.inherits(DiscountApi, pb.BaseController);

DiscountApi.prototype.render = function (mainCb) {
    var self = this;
    get = this.query,
        customObjectService = new pb.CustomObjectService(),
        objectName = 'discount';

    customObjectService.loadTypeByName(objectName, function (err, objectType) {
        if (!objectType) {
            pb.log.error('Custom object ' + objectName + ' does not exist');
            mainCb({
                content: pb.BaseController.apiResponse(pb.BaseController.API_FAILURE, null, discountResults)
            });
        } else {
            self.getFullCustomObjects(objectType, function (err, results) {
                if (err) {
                    mainCb({
                        content: pb.BaseController.apiResponse(pb.BaseController.API_FAILURE, null, err)
                    });
                } else {
                    mainCb({
                        content: pb.BaseController.apiResponse(pb.BaseController.API_SUCCESS, null, results)
                    });
                }
            });
        }
    });
};

DiscountApi.prototype.getFullCustomObjects = function (objectType, mainCb) {
    var self = this,
        customObjectService = new pb.CustomObjectService();

    customObjectService.findByType(objectType, null, function (err, objectResults) {
        self.getChildrenForArrayOfCustomObjects(objectResults, objectType, mainCb);
    });
};

DiscountApi.prototype.getChildrenForArrayOfCustomObjects = function (coArray, coType, mainCb) {
    var fullObjects = [],
        customObjectService = new pb.CustomObjectService();

    var getObjectChildren = function (customObject, cb) {
        var options = {
            fetch_depth: 1
        };

        customObjectService.fetchChildren(customObject, options, coType, function (err, fullCustomObj) {
            if (err) {
                cb(err);
            } else {
                fullObjects.push(fullCustomObj);
                cb();
            }
        })
    };

    async.each(coArray, getObjectChildren, function (err, results) {
        mainCb(err, fullObjects);
    });
};

DiscountApi.getRoutes = function (cb) {
    var routes = [{
        method: 'get',
        path: '/api/discounts',
        auth_required: true,
        content_type: 'application/json'
    }];

    cb(null, routes);
};


//exports
module.exports = DiscountApi;
