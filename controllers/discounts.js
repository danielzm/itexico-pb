/**
 * @author Daniel Zamorano, @danielzm, <daniel.zamorano.m@gmail.com>
 * @copyright 2015 Daniel Zamorano
 */

var Index = require('./index.js');

function Discounts() {}

//inheritance
util.inherits(Discounts, Index);

Discounts.prototype.render = function (cb) {
    var self = this;
    cb(pb.BaseController);
};

Discounts.prototype.render = function (cb) {
    var self = this;
    pb.content.getSettings(function (err, contentSettings) {
        self.gatherData(function (err, data) {
            self.ts.registerLocal('meta_lang', localizationLanguage);
            self.ts.registerLocal('current_url', self.req.url);
            self.ts.registerLocal('page_name', 'Discounts');
            self.ts.registerLocal('navigation', new pb.TemplateValue(data.nav.navigation, false));
            self.ts.registerLocal('account_buttons', new pb.TemplateValue(data.nav.accountButtons, false));
            self.ts.registerLocal('angular', function (flag, cb) {
                var objects = {
                    trustHTML: 'function(string){return $sce.trustAsHtml(string);}'
                };
                var angularData = pb.js.getAngularController(objects, ['ngSanitize']);
                cb(null, angularData);
            });
            self.ts.load('talent-management/discounts', function (err, result) {
                if (util.isError(err)) {
                    throw err;
                }

                cb({
                    content: result
                });
            });
        });
    });
};

Discounts.getRoutes = function (cb) {
    var routes = [{
        method: 'get',
        path: '/talent-management/discounts',
        auth_required: true,
        content_type: 'text/html'
    }];
    cb(null, routes);
};

//exports
module.exports = Discounts;
