/**
 * @author Daniel Zamorano, @danielzm, <daniel.zamorano.m@gmail.com>
 * @copyright 2015 Daniel Zamorano
 */

/**
 * Google Authentication
 */

var google = require('googleapis');
var GoogleAuthentication = require('../include/security/authentication/GoogleAuthentication.js');

var OAuth2 = google.auth.OAuth2,
    googleAuthConfig,
    oauth2Client,
    scopes,
    askUserPermissionURL;

function GoogleAuth() {}

//inheritance
util.inherits(GoogleAuth, pb.BaseController);

GoogleAuth.prototype.render = function (cb) {
    var self = this,
        userType = 'USER';

    if (self.req.url.match('admin_atempt=1')) {
        pb.log.debug('ADMIN Login atempt: ' + self.req.url);
        userType = 'ADMIN';
    }

    pb.log.debug('self.req.query: ', url.parse(self.req.url, true).query);

    self.initGoogleAuth(userType, function () {
        if (self.req.url === '/auth/google' || self.req.url === '/auth/google?admin_atempt=1') {
            //redirect to google auth page
            self.redirect(askUserPermissionURL, cb);
        } else if (self.req.url.match('/auth/google/callback')) {
            var query = url.parse(self.req.url, true).query,
                userType = query.state;

            oauth2Client.getToken(query.code, function (err, tokens) {
                if (!err) {
                    oauth2Client.setCredentials(tokens);

                    var oauth2 = google.oauth2({
                        version: 'v2',
                        auth: oauth2Client
                    });

                    // get user data
                    oauth2.userinfo.v2.me.get({}, function (err, data) {
                        if (err) {
                            pb.log.error("Google Auth Error: " + err.toString());
                            self.loginError(err.toString());
                            this.redirect('/user/login', cb);
                        }

                        pb.log.info("authenticated GoogleUser: " + data.email);

                        // next tick until we get all google data
                        process.nextTick(function () {
                            self.authenticateUser(userType, data, cb);
                        });

                    });
                } else {
                    pb.log.error(err.stack);
                    cb(err);
                }
            });
        }
    });
};

GoogleAuth.prototype.initGoogleAuth = function (userType, cb) {
    var env = process.env.NODE_ENV || 'dev';

    pb.log.debug('Loading URL for user: ' + userType);

    scopes = [
        'https://www.googleapis.com/auth/userinfo.email',
        'https://www.googleapis.com/auth/userinfo.profile'
    ];

    this.getAuthConfig(function (err, googleAuthConfig) {
        if (err) {
            cb(err);
            return;
        }
        oauth2Client = new OAuth2(googleAuthConfig[env].clientId, googleAuthConfig[env].clientSecret,
            googleAuthConfig[env].callbackURL);

        askUserPermissionURL = oauth2Client.generateAuthUrl({
            access_type: 'offline',
            scope: scopes,
            state: userType
        });

        cb();
    });
};

GoogleAuth.prototype.getAuthConfig = function (cb) {
    var configName = 'googleAuthConfig',
        configurations = {};

    pb.plugins.getSettings('itexico', function (err, configObj, other) {
        for (config in configObj) {
            if (configObj[config].name.match(configName)) {
                var parts = configObj[config].name.split('_');
                configurations[parts[0]] = configurations[parts[0]] || {};
                configurations[parts[0]][parts[2]] = configObj[config].value;
            } else if (configObj[config].name.match('Initial_Page_Url')) {
                configurations.afterLoginRedirectPage = configObj[config].value;
            }
        }
        cb(null, configurations);
    });
}

GoogleAuth.prototype.redirectConfig = function (cb) {
    var configName = 'Initial_Page_Url',
        afterLoginRedirectPage = '';

    pb.plugins.getSettings('itexico', function (err, configObj, other) {
        for (config in configObj) {
            if (configObj[config].name.match(configName)) {
                pb.log.debug(configObj[config]);
                afterLoginRedirectPage = configObj[config].value;
            }
        }
        cb(null, afterLoginRedirectPage);
    });
}
GoogleAuth.prototype.authenticateUser = function authenticateUser(userType, googleUser, cb) {
    var self = this;

    googleUser.access_level = (userType === 'USER') ? ACCESS_USER : ACCESS_WRITER;

    // authenticate session with google data
    pb.security.authenticateSession(self.session, googleUser, new GoogleAuthentication(),
        function (err, user) {
            if (!user && !err) {
                var errorCode = 'NO_USER_FOUND';

                if (googleUser.access_level > ACCESS_USER) {
                    errorCode = 'NO_ADMIN_USER_FOUND';
                }

                var err = {
                    access_level: googleUser.access_level,
                    code: errorCode,
                    data: googleUser.email,
                    stack: ''
                };
            }

            if (err) {
                if (err.stack) {
                    pb.log.error(err.stack);
                }
                self.loginError(err, cb);
            }

            self.redirectConfig(function (err, redirectPage) {
                //redirect
                var location = redirectPage || '/';
                if (googleUser.access_level > ACCESS_USER) {
                    location = '/admin';
                }
                if (self.session.on_login && self.session.on_login !== '/') {
                    if (self.session.on_login.match('/admin') && googleUser.access_level < ACCESS_WRITER) {
                        delete self.session.on_login;
                    } else {
                        location = self.session.on_login;
                        delete self.session.on_login;
                    }
                }
                self.redirect(location, cb);
                return;
            });
        }
    );
}

GoogleAuth.prototype.loginError = function (error) {
    var errorMap = {
            "INVALID_DOMAIN": "Invalid domain, please use an itexico account",
            "NO_USER_FOUND": "No user found with this email",
            "NO_ADMIN_USER_FOUND": "No admin user found with this email"
        },
        errorCode = error.code;

    this.session.error = errorMap[errorCode] + ' : ' + error.data || "Error: " + errorCode;
    return;
};

GoogleAuth.getRoutes = function (cb) {
    var routes = [{
        method: 'get',
        path: '/auth/google',
        auth_required: false,
        content_type: 'text/html'
    }, {
        method: 'get',
        path: '/auth/google/callback',
        auth_required: false,
        content_type: 'text/html'
    }];
    cb(null, routes);
};

//exports
module.exports = GoogleAuth;
