/*
    Copyright (C) 2014  PencilBlue, LLC

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

//dependencies
var Index = require('./index.js');
var jsdom = require("node-jsdom");
var ArticleService = require(path.join(DOCUMENT_ROOT, '/include/service/entities/article_service')).ArticleService;

/**
 * Loads a page
 * @class PageController
 * @constructor
 */
function PageController() {}

//inheritance
util.inherits(PageController, Index);

/**
 * Looks up a page and renders it
 * @see BaseController#render
 * @method render
 * @param {Function} cb
 */
PageController.prototype.render = function (cb) {
    var self = this;
    var custUrl = this.pathVars.customUrl;

    //check for object ID as the custom URL
    var doRedirect = false;
    var where = null;
    if (pb.validation.isIdStr(custUrl)) {
        where = {
            _id: pb.DAO.getObjectID(custUrl)
        };
        if (pb.log.isSilly()) {
            pb.log.silly("ArticleController: The custom URL was not an object ID [%s].  Will now search url field. [%s]", custUrl, e.message);
        }
    } else {
        where = {
            url: custUrl
        };
    }

    var dao = new pb.DAO();
    dao.loadByValues(where, 'page', function (err, page) {
        if (util.isError(err) || page == null) {
            if (where.url) {
                self.reqHandler.serve404();
                return;
            }

            dao.loadByValues({
                url: custUrl
            }, 'page', function (err, page) {
                if (util.isError(err) || page == null) {
                    self.reqHandler.serve404();
                    return;
                }

                self.renderPage(page, cb);
            });

            return;
        }
        

        // verify if it's private
        if (page.private_area) {
            pb.log.debug('Loading private page :: ' + page.headline);
            self.getPrivateArea(page.private_area, function (err, privateArea) {
                if (err) {
                    if (err.code = "PRIVATE_NOT_FOUND") {
                        pb.log.error('Private area id: ' + page.private_area + ' not found');
                        self.reqHandler.serve404();
                        return;
                    }
                    cb(err);
                    return;
                }

                var userFound = false,
                    actualUser = self.session.authentication.user;

                if (privateArea && privateArea['allowed_users']) {
                    privateArea['allowed_users'].forEach(function (user) {
                        if (user.email === actualUser.email) {
                            userFound = true;
                        }
                    });
                    if (userFound) {
                        pb.log.debug('Private page access for: ' + actualUser.email);
                        self.renderPage(page, cb);
                        return;
                    } else {
                        pb.log.debug('Denied access to private page for : ' + actualUser.email);
                        self.ts.setTheme('itexico');
                        self.serve403(actualUser.email);
                        return;
                    }
                } else {
                    pb.log.debug('Denied acces to private page for : ' + actualUser.email);
                    self.ts.setTheme('itexico');
                    self.serve403(actualUser.email);
                    return;
                }
            });
        } else {
            self.renderPage(page, cb);
        }
    });
};

// ToDo: we need a service that implements all the private area features implemented
// in several files and keep  the DRY concept

PageController.prototype.getPrivateArea = function (privateId, cb) {
    var self = this,
        customObjectService = new pb.CustomObjectService(),
        options = {
            fetch_depth: 1
        };

    customObjectService.loadById(privateId, {}, function (err, object) {
        if (err) {
            cb(err);
            return;
        } else if (!object) {
            cb({
                code: "PRIVATE_NOT_FOUND"
            });
            return;
        }
        customObjectService.fetchChildren(object, options, object.type, function (err, fullPrivateObj) {
            cb(err, fullPrivateObj);
        });
    });
};

PageController.prototype.renderPage = function (page, cb) {

    var self = this;

    // If has parent url, render with navigation
    if(true){

        var dao = new pb.DAO();
        var where = {url: page.parent_url}
        dao.loadByValues(where, 'page', function (err, parentPage){
            if(!parentPage){
                // Default rendering
                self.req.pencilblue_page = page._id.toString();
                self.page = page;
                self.setPageName(page.name);
                PageController.super_.prototype.render.apply(self, [cb]);
                return;
            }

            pb.content.getSettings(function (err, contentSettings) {
                self.gatherData(function (err, data) {
                    self.ts.registerLocal('meta_lang', localizationLanguage);
                    self.ts.registerLocal('current_url', self.req.url);
                    self.ts.registerLocal('page_name', 'Area');
                    self.ts.registerLocal('navigation', new pb.TemplateValue(data.nav.navigation, false));
                    self.ts.registerLocal('account_buttons', new pb.TemplateValue(data.nav.accountButtons, false));

                    self.ts.registerLocal('topics', function(flag, cb) {

                        // TODO Is it better to save the links as a property instead of rendering every time?
                        jsdom.env(
                            parentPage.page_layout ,
                            ["http://code.jquery.com/jquery.js"],
                            function (err, window) {

                                var htmlAreaNav = new Array();

                                htmlAreaNav.push("<h3><a href='" + pb.UrlService.urlJoin("/page/", parentPage.url));
                                htmlAreaNav.push("'>" + parentPage.headline + "</a></h3><ul>")

                                window.$("a").each(function(i, link){
                                    if(window.$(link).attr("href").indexOf("/page/") < 0) return; // Skip non page links
                                    if(!window.$(link).text()) return; // Skip undisplayed links
                                    var isSelectedPage = pb.UrlService.urlJoin("/page/", page.url) == window.$(link).attr("href");
                                    if( isSelectedPage ) htmlAreaNav.push("<b>");
                                    htmlAreaNav.push("<li>" + window.$(link)[0].outerHTML + "</li>")
                                    if( isSelectedPage ) htmlAreaNav.push("</b>");
                                });
                                htmlAreaNav.push("</ul>");
                                cb(err, new pb.TemplateValue(htmlAreaNav.join(''), false));

                            }
                        );
                    });

                    self.ts.registerLocal('pages', function(flag, cb) {
                        service = new ArticleService();
                        service.setContentType('page');
                        service.findById(page._id, function (err, res) {
                            self.renderContent(res[0], contentSettings, data.nav.themeSettings, 0, function(err, result) {
                                cb(err, new pb.TemplateValue(result, false));
                            });
                        });
                    });


                    self.ts.registerLocal('angular', function (flag, cb) {
                        var objects = {
                            trustHTML: 'function(string){return $sce.trustAsHtml(string);}'
                        };
                        var angularData = pb.js.getAngularController(objects, ['ngSanitize']);
                        cb(null, angularData);
                    });
                    self.ts.load('nav_page', function (err, result) {
                        if (util.isError(err)) {
                            throw err;
                        }
                        cb({ content: result });
                    });

                });
            });
        });

    // Else: default rendering
    } else {

        this.req.pencilblue_page = page._id.toString();
        this.page = page;
        this.setPageName(page.name);
        PageController.super_.prototype.render.apply(this, [cb]);

    }
};

/**
 * Serves up a 403 when the specified page is private and user has no rights on it
 * @method serve403
 */
PageController.prototype.serve403 = function (userEmail) {
    var AccessDenied = require('./error/403.js'),
        AccessDeniedInstance = new AccessDenied();

    this.reqHandler.doRender({}, AccessDeniedInstance, {});

    if (pb.log.isSilly()) {
        pb.log.silly("Authorization Required, Sending 403 for user:" + userEmail + " URL=" + this.url.href);
    }
};

/**
 * Retrieves the name of the page.  The page's headhile
 *
 */
PageController.prototype.getPageTitle = function () {
    return this.page.headline;
};

PageController.getRoutes = function (cb) {
    var routes = [{
        method: 'get',
        path: '/page',
        auth_required: true,
        content_type: 'text/html'
    }, {
        method: 'get',
        path: '/page/:id',
        auth_required: true,
        content_type: 'text/html'
    }];
    cb(null, routes);
};

//exports
module.exports = PageController;
