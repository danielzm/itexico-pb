'use strict';
/**
 * @author Daniel Zamorano, @danielzm, <daniel.zamorano.m@gmail.com>
 * @copyright 2015 Daniel Zamorano
 */

var Index = require('./index.js');

function Welcome() {}

//inheritance
util.inherits(Welcome, Index);

Welcome.prototype.render = function (cb) {
    var self = this;
    cb(pb.BaseController);
};

Welcome.prototype.render = function (cb) {
    var self = this;
    pb.content.getSettings(function (err, contentSettings) {
        self.gatherData(function (err, data) {
            self.ts.registerLocal('meta_lang', localizationLanguage);
            self.ts.registerLocal('current_url', self.req.url);
            self.ts.registerLocal('page_name', 'Welcome');
            self.ts.registerLocal('navigation', new pb.TemplateValue(data.nav.navigation, false));
            self.ts.registerLocal('account_buttons', new pb.TemplateValue(data.nav.accountButtons, false));
            self.ts.registerLocal('angular', function (flag, cb) {
                var objects = {
                    trustHTML: 'function(string){return $sce.trustAsHtml(string);}'
                };
                var angularData = pb.js.getAngularController(objects, ['ngSanitize']);
                cb(null, angularData);
            });
            self.ts.load('welcome', function (err, result) {
                if (util.isError(err)) {
                    throw err;
                }

                cb({
                    content: result
                });
            });
        });
    });
};

Welcome.getRoutes = function (cb) {
    var routes = [
    {
        method: 'get',
        path: '/',
        auth_required: true,
        content_type: 'text/html'
    },
    {
        method: 'get',
        path: '/welcome',
        auth_required: true,
        content_type: 'text/html'
    }];
    cb(null, routes);
};

//exports
module.exports = Welcome;
