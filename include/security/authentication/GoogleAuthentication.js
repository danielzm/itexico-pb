/**
 * @author Daniel Zamorano, @danielzm, <daniel.zamorano.m@gmail.com>
 * @copyright 2015 Daniel Zamorano
 */

function GoogleAuthentication() {}

GoogleAuthentication.prototype.authenticate = function (GoogleUserObject, cb) {
    pb.log.debug('Got google user to authenticate', GoogleUserObject);

    var self = this;
    if (!pb.utils.isObject(GoogleUserObject)) {
        cb(new Error("The GoogleUserObject parameter must be an object: " +
                GoogleUserObject),
            null);
        return;
    }

    self.isValidUser(GoogleUserObject, function (err, isValid) {
        if (err) {
            pb.log.debug('authenticate Error:' + err.code);
            cb(err);
        } else if (isValid) {
            pb.log.debug('Is a valid user:' + GoogleUserObject.email);
            //build query
            var query = {
                email: GoogleUserObject.email,
                admin: {
                    '$gte': GoogleUserObject.access_level
                }
            };
            //search for user
            var dao = new pb.DAO();
            dao.loadByValues(query, 'user', function (err, user) {
                if (err) {
                    pb.log.debug('Database error: ' + err.toString());
                    cb(err);
                    return;
                    // we just store writers > admins in the db so ...
                    // in here if user is not in the db that just means is not an admin
                    // but if he is using a valid domain then he has automatic access
                    // as normal user so, we just map the google user as PB user
                    // to let him login
                } else if (!user && GoogleUserObject.access_level == ACCESS_USER) {
                    pb.log.debug('User not found in DB');
                    var userDocument = pb.DocumentCreator.create('user', self.mapGoogleUserToPBUser(GoogleUserObject));
                    cb(null, userDocument);
                    return;
                }

                pb.log.debug('User found in DB');
                cb(err, user);
                return;
            });

        } else {
            pb.log.info('Invalid User: ' + GoogleUserObject.email);
            cb({
                data: GoogleUserObject.email,
                access_level: GoogleUserObject.access_level,
                code: 'INVALID_DOMAIN'
            });
        }
    });


    return;
};

GoogleAuthentication.prototype.mapGoogleUserToPBUser = function (googleUserObject) {
    // default attrs
    var pbUserObject = {
            permissions: [],
            admin_level: 0,
            admin: 0, // just reader access
        },
        mapGoogleToPBAttrs = {
            // pencilblue : // google
            '_id': 'id',
            'first_name': 'given_name',
            'last_name': 'family_name',
            'email': 'email',
            'username': 'email', // not quite sue it's the best,
            'gplus': 'link',
            'profile_picture': 'picture',
            'locale': 'locale',
            'domain': 'hd'
        };

    for (var pbAttr in mapGoogleToPBAttrs) {
        var googleAttr = mapGoogleToPBAttrs[pbAttr];
        pbUserObject[pbAttr] = googleUserObject[googleAttr] || null;
    }
    return pbUserObject;
}

GoogleAuthentication.prototype.isValidUser = function isValidUser(googleUser, cb) {
    if (!googleUser) {
        pb.log.error('isValidUser() Error: No googleUser provided');
        return false;
    }

    pb.plugins.getSettings('itexico', function (err, configObj) {
        var configFound = false,
            validDomain = false;
        if (err) {
            pb.log.debug('getSettings Error: ' + err.toString());
            cb(err);
        }
        for (index in configObj) {
            var config = configObj[index];
            if (config.name == 'Login_Allowed_Domains') {
                configFound = true;
                var domains = config.value.split(',');

                var cleanDomains = [];
                domains.forEach(function (domain, index, array) {
                    cleanDomains.push(domain.replace(/\s/g, ''));
                });

                if (cleanDomains.indexOf(googleUser.hd) > -1) {
                    validDomain = true;
                }
            }
        }
        if (!configFound) {
            cb({
                code: 'NO_LOGIN_ALLOWED_DOMAINS'
            });
        } else {
            cb(null, validDomain);
        }

    });
};

//exports
module.exports = GoogleAuthentication;
