/**
 * iTexico - A iTexico theme for PencilBlue
 *
 * @author Daniel Zamorano, @danielzm, <daniel.zamorano.m@gmail.com>
 * @copyright 2015 Daniel Zamorano
 */
function iTexico() {}

/**
 * Called when the application is being installed for the first time.
 *
 * @param cb A callback that must be called upon completion.  cb(err, result).
 * The result is ignored
 */
iTexico.onInstall = function (cb) {
    var self = this;

    // ToDo: Work with async here to create a chain of create custom objects
    // DRY concept

    var customObjectError = function (err) {
        pb.log.error("Custom Object Error: " + err.toString());
        cb(err);
    };

    // first create a category object as discount depends on this
    self.createCategoryCustomObject(function (err, objectType) {
        if (err) {} else {
            self.createDiscountCustomObject(function (err, objectType) {
                if (err) {
                    customObjectError(err);
                } else {
                    self.createPrivateAreaCustomObject(function (err, objectType) {
                        if (err) {
                            customObjectError(err);
                        } else {
                            self.createSpecialContentCustomObject(function (err, objectType) {
                                if (err) {
                                    customObjectError(err);
                                } else {
                                    self.createCarouselCustomObject(function (err, objectType) {
                                        if (err) {
                                            customObjectError(err);
                                        } else {
                                            self.createNewsCustomObject(function (err, objectType) {
                                                if (err) {
                                                    customObjectError(err);
                                                } else {
                                                    cb(null, true);
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};

iTexico.createCategoryCustomObject = function (cb) {
    var objectName = 'category_discount',
        objectModel = {
            name: objectName,
            fields: {
                enabled: {
                    field_type: 'boolean'
                }
            }
        };

    this.createCustomObject(objectName, objectModel, cb);
    return;
};

iTexico.createSpecialContentCustomObject = function (cb) {
    var objectName = 'special_content',
        objectModel = {
            name: objectName,
            fields: {
                headline: {
                    field_type: 'text'
                },
                subheading: {
                    field_type: 'text'
                },
                content: {
                    field_type: 'wysiwyg'
                }
            }
        };

    this.createCustomObject(objectName, objectModel, cb);
    return;
};

iTexico.createDiscountCustomObject = function (cb) {
    var objectName = 'discount',
        objectModel = {
            name: objectName,
            fields: {
                enabled: {
                    field_type: 'boolean'
                },
                percentage: {
                    field_type: 'number'
                },
                category: {
                    field_type: "peer_object",
                    object_type: "custom:category_discount"
                },
                start_date: {
                    field_type: 'date'
                },
                expiring_date: {
                    field_type: 'date'
                },
                image: {
                    field_type: "child_objects",
                    object_type: "media"
                },
                description: {
                    field_type: 'wysiwyg'
                },
                media: {
                    field_type: "child_objects",
                    object_type: "media"
                }
            }
        };

    this.createCustomObject(objectName, objectModel, cb);
    return;
};

iTexico.createPrivateAreaCustomObject = function (cb) {
    var objectName = 'private_area',
        objectModel = {
            name: objectName,
            fields: {
                enabled: {
                    field_type: 'boolean'
                },
                "url-/area/": {
                    field_type: 'text'
                },
                allowed_users: {
                    field_type: 'child_objects',
                    object_type: 'user'
                }
            }
        };

    this.createCustomObject(objectName, objectModel, cb);

    return;
};

iTexico.createCarouselCustomObject = function (cb) {
    var objectName = 'carousel_items',
        objectModel = {
            name: objectName,
            fields: {
                caption: {
                    field_type: 'text'
                },
                url: {
                    field_type: 'text'
                },
                priotity: {
                    field_type: 'number'
                },
                image: {
                    field_type: "child_objects",
                    object_type: "media"
                }
            }
        };

    this.createCustomObject(objectName, objectModel, cb);
    return;
};

iTexico.createNewsCustomObject = function (cb) {
    var objectName = 'news',
        objectModel = {
            name: objectName,
            fields: {
                headline: {
                    field_type: 'text'
                },
                priotity: {
                    field_type: 'number'
                },
                article: {
                    field_type: "child_objects",
                    object_type: "article"
                },
                thumbnail: {
                    field_type: "child_objects",
                    object_type: "media"
                }
            }
        };

    this.createCustomObject(objectName, objectModel, cb);
    return;
};

iTexico.createCustomObject = function (objectName, objectModel, cb) {
    var customObjectService = new pb.CustomObjectService();

    customObjectService.loadTypeByName(objectName, function (err, objectType) {
        if (!objectType) {
            pb.log.info('Creating ' + objectName + ' custom object');
            customObjectService.saveType(objectModel, function (err, objectType) {
                if (!err) {
                    pb.log.info(objectName + ' created.');
                    cb(null, objectType);
                } else {
                    cb(err);
                }
            });
        } else {
            pb.log.debug(objectName + ' existent, nothing to do.');
            cb(null, objectType);
        }
    });
};
/**
 * Called when the application is uninstalling this plugin.  The plugin should
 * make every effort to clean up any plugin-specific DB items or any in function
 * overrides it makes.
 *
 * @param cb A callback that must be called upon completion.  cb(err, result).
 * The result is ignored
 */
iTexico.onUninstall = function (cb) {
    cb(null, true);
};

/**
 * Called when the application is starting up. The function is also called at
 * the end of a successful install. It is guaranteed that all core PB services
 * will be available including access to the core DB.
 *
 * @param cb A callback that must be called upon completion.  cb(err, result).
 * The result is ignored
 */
iTexico.onStartup = function (cb) {
    pb.AdminSubnavService.registerFor('plugin_settings', function (navKey, localization, plugin) {
        if (plugin.uid === 'iTexico') {
            return [{
                name: 'home_page_settings',
                title: 'Home page settings',
                icon: 'home',
                href: '/admin/plugins/itexico_theme/settings/home_page'
            }];
        }
        return [];
    });
    cb(null, true);
};

/**
 * Called when the application is gracefully shutting down.  No guarantees are
 * provided for how much time will be provided the plugin to shut down.
 *
 * @param cb A callback that must be called upon completion.  cb(err, result).
 * The result is ignored
 */
iTexico.onShutdown = function (cb) {
    cb(null, true);
};

//exports
module.exports = iTexico;
