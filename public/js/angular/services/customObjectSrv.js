'use strict';
/**
 * @author Daniel Zamorano, @danielzm, <daniel.zamorano.m@gmail.com>
 * @copyright 2015 Daniel Zamorano
 */

angular.module('pencilblueApp')
    .service('customObjectSrv', function ($http) {
        var API = 'api/custom_object/';

        var get = function (objectName) {
            if (!objectName) {
                console.error('No custom object name provided.');
                return;
            }
            return $http.get(API + objectName)
        };

        // Service API
        this.get = get;
    });
