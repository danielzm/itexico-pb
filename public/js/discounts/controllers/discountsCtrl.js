/* global $*/
'use strict';

/**
 * Controller of the discounts
 */
angular.module('pencilblueApp')
    .directive('onFinishRender', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                if (scope.$last === true) {
                    $timeout(function () {
                        scope.$emit('ngRepeatFinished');
                    });
                }
            }
        };
    })
    .controller('DiscountsController', ['$scope', 'discountsSrv', function ($scope, $discountsSrv) {
        var $grid = $('#grid'),
            $filters = $('.filter-categories button');

        var cleanAllFilters = function () {
                $filters.each(function (index, el) {
                    $(el).removeClass('active');
                });
            },

            categoryFilter = function (event) {
                event.preventDefault();
                var category = $(this).attr('data-group');

                cleanAllFilters();

                $(this).addClass('active');

                if (category !== 'reset') {
                    $grid.shuffle('shuffle', category);
                    return;
                } else {
                    $grid.shuffle('shuffle', '');
                }

                return;
            };

        $scope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {
            $grid.shuffle({
                itemSelector: '.picture-item'
            });
        });

        $filters.on('click', categoryFilter);

        // work in the modal
        $scope.modalDetails = function ($event) {
            var $element = $event.currentTarget,
                discountObj = $discountsSrv.getById($element.dataset.discountId),
                mediaSrc = null,
                imageSrc = null;

            if (discountObj.media.length > 0) {
                mediaSrc = discountObj.media[0].location;
            }
            if (discountObj.image.length > 0) {
                imageSrc = discountObj.image[0].location;
            }

            _updateModal({
                title: discountObj.name + " " + discountObj.percentage + "% off!",
                imageSrc: imageSrc,
                category: discountObj.category.name,
                mediaSrc: mediaSrc,
                description: discountObj.description
            });

            $('#details-modal').modal('show');
        };

        // This is needed cause bootstrap and angular do not play well together
        // need to implement angular version bootstrap
        // https://angular-ui.github.io/bootstrap/
        var _updateModal = function (modalData) {
            var $modal = $('#details-modal'),
                $modalLogo = $modal.find('img.logo'),
                $modalSubheader = $modal.find('h5'),
                $modalDescription = $modal.find('div.description'),
                $modalMedia = $modal.find('a.link-media');

            if (modalData.mediaSrc) {
                $modalMedia.attr('href', modalData.mediaSrc);
                $modalMedia.addClass('enabled');
            } else {
                $modalMedia.attr('href', "#");
                $modalMedia.removeClass('enabled');
            }
            if (modalData.imageSrc) {
                $modalLogo.attr('src', modalData.imageSrc);
                $modalLogo.removeClass('no-logo');
            } else {
                $modalLogo.attr('src', "#");
                $modalLogo.addClass('no-logo');
            }

            $modalLogo.attr('src', modalData.imageSrc);
            $modalSubheader.html(modalData.title);
            $modalDescription.html(modalData.description);
            return;
        };

        // get disscounts and initialize shuffle plugin
        $discountsSrv.init(function (data) {
            $scope.discounts = data;
        });
        $discountsSrv.getAll()

    }]);
