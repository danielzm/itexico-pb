'use strict';

/**
 *
 */
angular.module('pencilblueApp')
    .service('discountsSrv', function ($http) {
        var cache = [];
        var API = '/api/discounts';

        var _fetch = function (callback) {
            if (cache.length === 0) {
                $http.get(API)
                    .success(function (response) {
                        cache = response.data;
                        if (callback) {
                            return callback(cache);
                        }
                        return;
                    }).error(function (response, status, headers, config) {
                        console.error("Error on getting discounts", response);
                    });
            }
        };

        this.getAll = function () {
            return cache;
        };

        var _mapDiscountsById = function (discounts) {
            var map = {};
            for (var index in discounts) {
                var id = discounts[index]._id;
                map[id] = discounts[index];
            }
            return map;
        };

        this.getById = function (id) {
            return _mapDiscountsById(this.getAll())[id] || null;
        };

        this.init = function (callback) {
            _fetch(callback);
        };

        window.test = this;
    });
