/* global $*/
'use strict';
/**
 * @author Daniel Zamorano, @danielzm, <daniel.zamorano.m@gmail.com>
 * @copyright 2015 Daniel Zamorano
 */

 angular.module('pencilblueApp')
 .controller('WelcomeController', ['$scope', 'customObjectSrv', function ($scope, $customObjectSrv) {
    
    // scope objects
    $scope.welcome = {};
    $scope.quickLinks = {};
    $scope.carouselItems = {};
    $scope.news = {};


    angular.element(document).ready(function () {
        $('.carousel').carousel({
            interval: 6000
        });

        // Twitter widget
        !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
    }); 

    $customObjectSrv.get('special_content')
    .success(function (response) {
        if (response.data && response.data instanceof Array)
            response.data.map(function (customObject) {
                switch(customObject.name){
                    case 'welcome': 
                    $scope.welcome = customObject; break;
                    case 'quick_links': 
                    $scope.quickLinks = customObject; break;
                }
                return null;
            });
        return;
    }).error(function (response, status, headers, config) {
        console.error("Error getting data", response);
    });


    $customObjectSrv.get('carousel_items')
    .success(function (response) {
        $scope.carouselItems = response.data.sort(sortByPriority);
    }).error(function (response, status, headers, config) {
        console.error("Error on getting carousel_items", response);
    });


    $customObjectSrv.get('news')
    .success(function (response) {
        $scope.news = response.data.sort(sortByPriority);
    }).error(function (response, status, headers, config) {
        console.error("Error on getting carousel_items", response);
    });


    var sortByPriority = function(a ,b){
       return (a.priority && b.priority) ? a.priority - b.priority :
             a.priority ? a.priority : b.priority ? b.priority : 0 ;
    }


    $scope.striphtml = function (html){
       var tmp = document.createElement("DIV");
       tmp.innerHTML = html;
       return tmp.textContent || tmp.innerText || "";
    }

}]);
